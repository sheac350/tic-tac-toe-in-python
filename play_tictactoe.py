# Imports
import game
import the_AI

# Constants for the menu choices
SHOW_GAME_RULES = 1
PLAY_GAME = 2
ENTER_PLAYER_ID = 3
AI_DIFFICULTY = 4
SEARCH_PLAYER_ID = 5
QUIT_CHOICE = 6

# Ai difficulty level choices
EASY = 1
MEDIUM = 2
HARD = 3


# The main function.
def main():
    # The choice variable controls the loop
    # and holds the user's menu choice.
    choice = 0
    ai_level = 2
    player_id = ''

    while choice != QUIT_CHOICE:
        try:
            # display the menu.
            display_menu()

            # Get the user's choice.
            choice = int(input('Enter your choice: '))

            # Perform the selected action.
            if choice == SHOW_GAME_RULES:
                display_game_rules()
            elif choice == PLAY_GAME:
                play_game(ai_level, player_id)
            elif choice == ENTER_PLAYER_ID:
                player_id = enter_player_id()
            elif choice == AI_DIFFICULTY:
                ai_level = get_ai_level()
            elif choice == SEARCH_PLAYER_ID:
                search_players()
            elif choice == QUIT_CHOICE:
                print('Exiting the program...')
            else:
                print('Error: invalid selection.')

        except ValueError:
            print('\nA number is required, please enter a valid number.')


def get_ai_level():
    # Function that returns the chosen ai level
    choice = 0

    # Input validation
    while choice < 1 or choice > 3:
        print('\nAI Difficulty Levels: ')
        print('1) Easy')
        print('2) Medium')
        print('3) Hard')

        try:
            # Get the user's choice
            choice = int(input('Please enter your choice of AI difficulty: '))

            # If statement returns value depending on choice
            if choice == EASY:
                return choice
            elif choice == MEDIUM:
                return choice
            elif choice == HARD:
                return choice
            else:
                print('\nPlease enter a valid number.')

        except ValueError:
            print('\nA number is required, please enter a valid number.')


def enter_player_id():
    # Function that returns the player id that the user has inputted
    valid = False

    # Input validation
    while not valid:
        try:
            # Get the user's choice
            player_id = input('\nPlease enter your preferred Player ID: ')

            # if statement checks whether entered player id already exists
            if game.search_player_records(player_id):
                print('This User ID is already taken, please enter another one.')
            # checks if user entered a string value of reasonable length
            elif len(player_id) < 2 or len(player_id) > 20:
                print('Your ID must be 2 - 20 characters long.')
            else:
                valid = True
                return player_id

        except ValueError:
            print('\nPlease enter a valid Player ID.')


def search_players():
    # function searches the entered player id, checks if it exists
    valid = False

    while not valid:
        try:
            # Get the user's choice
            player_id = input('\nPlease enter the Player ID you wish to search for: ')

            # search_player_records returns a true if the searched player_id is found
            if game.search_player_records(player_id):
                # getting the list of moves from the searched game
                previous_moves = game.get_recorded_moves_from_file(player_id)
                # validation
                valid = True

                # calling the replay game function and passing in the previous moves
                # when replay game is loaded if first asks the user if they want to replay the game
                replay_game(previous_moves)
            else:
                print('This Player ID was not found')
                # Get the user's choice
                # this gives the user a chance to exit this menu if they cannot find a player_id
                choice = input('Do you wish to exit? Enter Y for yes')

                if choice == 'Y' or choice == 'y':
                    valid = True

        except ValueError:
            print('\nPlease enter a valid Player ID.')


def display_menu():
    # function that displays the main menu
    print('\n      MENU')
    print('1) Show Game Rules')
    print('2) Play and Save New Game')
    print('3) Enter you preferred Game ID')
    print('4) Select AI difficulty level')
    print('5) Search a Game ID and watch a replay')
    print('6) Quit Game')


def display_game_rules():
    # function that displays the game rules
    print("\n1. The game is played on a grid that's 3 squares by 3 squares.")
    print("2. You are X, the computer is O. Players take turns putting their marks in empty squares.")
    print("3. The first player to get 3 of her marks in a row (up, down, across, or diagonally) is the winner.")
    print("4. When all squares are full, the game is over. If no player has 3 marks in a row, the game ends in a tie.")
    input("\nPress enter to continue")


def play_game(selected_ai_level, selected_player_id):
    # the main game function, takes two parameter values based on chosen ai level and player id

    # setting up variables based on returned values from functions in the game.py file
    the_8_game_lines = game.get_game_lines()
    previous_moves = game.get_list_of_previous_moves()
    game_situation = game.get_game_state(the_8_game_lines)
    player = game.PLAYER_1
    # variables are assigned based on passed in values
    ai_level = selected_ai_level
    player_id = selected_player_id

    # the game loop
    while game_situation == game.GAME_IN_PROGRESS:

        # displaying the board at the start of each iteration
        print('\nThe Game Position\n')
        print(game.get_tictactoe(), '\n')
        print('Current AI Level: ' + str(ai_level))

        # depending on if its an X or O turn, the player or the ai makes a move
        if player == game.PLAYER_1:
            human_move = get_move(previous_moves)
            game.writing_symbol(the_8_game_lines, human_move, game.PLAYER_1)
        else:
            ai_move = the_AI.get_whatever(ai_level, the_8_game_lines, previous_moves)
            game.writing_symbol(the_8_game_lines, ai_move, game.PLAYER_2)

        # switching from the current player to the opposite player
        if player == game.PLAYER_1:
            player = game.PLAYER_2
        else:
            player = game.PLAYER_1

        # getting the game state at the end of the loop
        game_situation = game.get_game_state(the_8_game_lines)

    # showing the end result of the game
    print('\nEnd of game, the result is: ', game_situation)
    print('\nThe Game Position')
    print(game.get_tictactoe(), '\n')

    # if a player_id has not been entered then the user is prompted to enter one
    if player_id == '':
        # user is prompted to enter a player_id
        player_id = enter_player_id()
        # saving a record of the game to file
        game.save_record_of_game(previous_moves, player_id, ai_level)
    else:
        # saving a record of the game to file
        game.save_record_of_game(previous_moves, player_id, ai_level)

    # resetting the game board, moves so far, and the simple game board
    game.reset_grid(the_8_game_lines)
    game.clear_moves_so_far()
    game.simple_board = game.reset_simple_board()


def replay_game(moves):
    # function that replays a game move by move from a saved file
    # getting the 8 game lines
    the_8_game_lines = game.get_game_lines()

    print('Game linked with Player ID has been found.')

    try:
        # get the users choice to replay the game or not
        choice = input('Do you want to replay this game? Press Y for yes or any key to exit.')

        if choice == 'Y' or choice == 'y':

            # for loop that replays the game move by move
            for index, move in enumerate(moves):
                # print that shows the current move
                print('Current move: ', (index + 1))

                # using the modulus operator to determine if an index value is odd or even
                # player is even index moves and ai is odd index moves
                if index % 2 == 0:
                    # assigning human move and writing the symbol to the game board
                    human_move = int(move)
                    game.writing_symbol(the_8_game_lines, human_move, game.PLAYER_1)

                    # displaying the game board after mark has been placed
                    print('\nThe Game Position\n')
                    print(game.get_tictactoe(), '\n')

                    # input so the user can control the replay properly
                    input()

                else:
                    # assigning ai move and writing the symbol to the game board
                    ai_move = int(move)
                    game.writing_symbol(the_8_game_lines, ai_move, game.PLAYER_2)

                    # displaying the game board after mark has been placed
                    print('\nThe Game Position\n')
                    print(game.get_tictactoe(), '\n')

                    # input so the user can control the replay properly
                    input()
        else:
            print('Returning to menu...')

    except ValueError:
        print('An error has occurred, please enter a valid character.')


def get_move(list_of_previous_moves_in_current_game):
    # function that gets the player move

    # setting up variables
    player_move = 0
    set_of_previous_moves = set(list_of_previous_moves_in_current_game)
    set_of_valid_moves = {1, 2, 3, 4, 5, 6, 7, 8, 9} - set_of_previous_moves
    valid_move_chosen = False

    while not valid_move_chosen:
        print('Please choose from the following moves: ', set_of_valid_moves)

        try:
            # getting the players choice of move
            player_move = int(input('\nEnter a number corresponding to the square you wish to pick: '))

            # checks if move is valid
            if player_move in set_of_valid_moves:
                valid_move_chosen = True
            else:
                print('\nInvalid square chosen, please enter a valid move.')

        except ValueError:
            print('A number is required, please enter a valid number.')

    return player_move


# Call the main function.
main()
