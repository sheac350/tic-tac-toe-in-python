# Imports
import csv

# COMPLETE THE FOLLOWING...
# NAMED_CONSTANTS for the 8 lines in a game of tic-tact-toe
TOP_ROW = 0
MIDDLE_ROW = 1
BOTTOM_ROW = 2
LEFT_COLUMN = 3
MIDDLE_COLUMN = 4
RIGHT_COLUMN = 5
UP_DIAGONAL = 6
DOWN_DIAGONAL = 7

# NAMED_CONSTANTS for the 9 cells in a game of tic-tact-toe
TOP_LEFT_CELL = 1
TOP_MIDDLE_CELL = 2
TOP_RIGHT_CELL = 3
MIDDLE_LEFT_CELL = 4
MIDDLE_CELL = 5
MIDDLE_RIGHT_CELL = 6
BOTTOM_LEFT_CELL = 7
BOTTOM_MIDDLE_CELL = 8
BOTTOM_RIGHT_CELL = 9


# Store the NAMED_CONSTANTS for the 8 lines in a set
GAME_LINE_NAMES = {TOP_ROW, MIDDLE_ROW, BOTTOM_ROW, LEFT_COLUMN, MIDDLE_COLUMN, RIGHT_COLUMN,
                   UP_DIAGONAL, DOWN_DIAGONAL}

# COMPLETE THE FOLLOWING...
# NAMED_CONSTANTS for the 3 cells/squares in any line in the game
FIRST = 0
SECOND = 1
THIRD = 2

# Store the NAMED_CONSTANTS for each line's 3 squares/cells in a set
CELL_NAMES = {FIRST, SECOND, THIRD}

# Game 'results' - these are String constants

# X_WIN = 'X has won!'
# O_WIN = 'O has won!'
# DRAW = 'The game was a draw!'
# GAME_IN_PROGRESS = 'The game is currently in progress.'

GAME_IN_PROGRESS = 'The game is currently in progress'
X_WIN = 'X has won the game'
O_WIN = 'O has won the game'
DRAW = 'The game was a draw'

GAME_STATES = {X_WIN, O_WIN, DRAW, GAME_IN_PROGRESS}

PLAYER_1 = 'X'
PLAYER_2 = 'O'
EMPTY_CELL_MARK = ''

# The SET of game results

# Create a list of the 8 game lines.
# Each Line is a list of 3 values.
game_lines = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' '],
              [' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]

# List to keep track of the moves that have been made in a game
moves_so_far = []

# A simple list for the AI to more easily seek out wins and avoid loss
# This is NOT the main game board
simple_board = [' ' for cell in range(10)]


def get_game_lines():

    # # In the next line replace '2' with your set named above @ code line 10
    # for each_line in GAME_LINE_NAMES:
    #
    #     # In the next line replace '3' with your set named above @ code line 19
    #     for cell in CELL_NAMES:
    #         lines[each_line][cell] = EMPTY_CELL_MARK
    #

    # Return the game lines.
    return game_lines


def place_symbol(p_grid, p_line, p_cell, p_symbol):
    # This function marks a symbol in the game lines
    # This depends on which values have been passed into its parameters
    p_grid[p_line][p_cell] = p_symbol


def writing_symbol(p_grid, chosen_cell, p_symbol):
    # This function sorts out which symbol should be marked in which game lines
    moves_so_far.append(chosen_cell)

    # The simple board is updated along with the main board as the game is played
    simple_board[chosen_cell] = p_symbol

    # The if statement below decides which game lines to write a symbol in depending on the chosen cell
    if chosen_cell == TOP_LEFT_CELL:
        place_symbol(p_grid, TOP_ROW, FIRST, p_symbol)
        place_symbol(p_grid, LEFT_COLUMN, FIRST, p_symbol)
        place_symbol(p_grid, DOWN_DIAGONAL, FIRST, p_symbol)
    elif chosen_cell == TOP_RIGHT_CELL:
        place_symbol(p_grid, TOP_ROW, THIRD, p_symbol)
        place_symbol(p_grid, RIGHT_COLUMN, FIRST, p_symbol)
        place_symbol(p_grid, UP_DIAGONAL, THIRD, p_symbol)
    elif chosen_cell == BOTTOM_LEFT_CELL:
        place_symbol(p_grid, BOTTOM_ROW, FIRST, p_symbol)
        place_symbol(p_grid, LEFT_COLUMN, THIRD, p_symbol)
        place_symbol(p_grid, UP_DIAGONAL, FIRST, p_symbol)
    elif chosen_cell == BOTTOM_RIGHT_CELL:
        place_symbol(p_grid, BOTTOM_ROW, THIRD, p_symbol)
        place_symbol(p_grid, RIGHT_COLUMN, THIRD, p_symbol)
        place_symbol(p_grid, DOWN_DIAGONAL, THIRD, p_symbol)

    elif chosen_cell == TOP_MIDDLE_CELL:
        place_symbol(p_grid, TOP_ROW, SECOND, p_symbol)
        place_symbol(p_grid, MIDDLE_COLUMN, FIRST, p_symbol)
    elif chosen_cell == MIDDLE_LEFT_CELL:
        place_symbol(p_grid, MIDDLE_ROW, FIRST, p_symbol)
        place_symbol(p_grid, LEFT_COLUMN, SECOND, p_symbol)
    elif chosen_cell == MIDDLE_RIGHT_CELL:
        place_symbol(p_grid, MIDDLE_ROW, THIRD, p_symbol)
        place_symbol(p_grid, RIGHT_COLUMN, SECOND, p_symbol)
    elif chosen_cell == BOTTOM_MIDDLE_CELL:
        place_symbol(p_grid, BOTTOM_ROW, SECOND, p_symbol)
        place_symbol(p_grid, MIDDLE_COLUMN, THIRD, p_symbol)

    elif chosen_cell == MIDDLE_CELL:
        place_symbol(p_grid, MIDDLE_ROW, SECOND, p_symbol)
        place_symbol(p_grid, MIDDLE_COLUMN, SECOND, p_symbol)
        place_symbol(p_grid, UP_DIAGONAL, SECOND, p_symbol)
        place_symbol(p_grid, DOWN_DIAGONAL, SECOND, p_symbol)


def reset_grid(p_grid):
    # This resets the main game grid
    for each_line in GAME_LINE_NAMES:
        for each_square in CELL_NAMES:
            p_grid[each_line][each_square] = ' '


def reset_simple_board():
    # This resets the simple game board
    for cell in range(10):
        simple_board[cell] = ' '
    return simple_board


# def check_win(p_grid):
#     for each_line in GAME_LINE_NAMES:
#         if len(set(p_grid[each_line])) == 1 and set(p_grid[each_line] != ' '):
#             print('There was a Winner!')
#             return True
#         else:
#             print('No Winner!')
#             return False


def simple_check_for_winner(board, mark):
    # This checks for win conditions on the simple board
    return ((board[7] == mark and board[8] == mark and board[9] == mark) or  # across bottom
            (board[4] == mark and board[5] == mark and board[6] == mark) or  # across middle
            (board[1] == mark and board[2] == mark and board[3] == mark) or  # across top
            (board[7] == mark and board[4] == mark and board[1] == mark) or  # down left side
            (board[8] == mark and board[5] == mark and board[2] == mark) or  # down middle
            (board[9] == mark and board[6] == mark and board[3] == mark) or  # down right side
            (board[7] == mark and board[5] == mark and board[3] == mark) or  # up diagonal
            (board[9] == mark and board[5] == mark and board[1] == mark))    # down diagonal


def get_game_state(p_grid):
    # This function gets the current game state
    game_state = GAME_IN_PROGRESS

    # If statement that sets the game state to a new value depending on which symbol won
    for each_line in p_grid:
        if each_line[FIRST] == PLAYER_1 and each_line[SECOND] == PLAYER_1 and each_line[THIRD] == PLAYER_1:
            game_state = X_WIN
            print('X won')

        elif each_line[FIRST] == PLAYER_2 and each_line[SECOND] == PLAYER_2 and each_line[THIRD] == PLAYER_2:
            game_state = O_WIN
            print('O won')

    # If statement that sets the game state to a draw when applicable
    if len(moves_so_far) == 9 and game_state == GAME_IN_PROGRESS:
        print('Game was a draw')
        game_state = DRAW

    return game_state


def get_list_of_previous_moves():
    # Function that returns the list of moves so far
    return moves_so_far


def get_tictactoe():
    # Function that displays the game board
    return '-------------' + '\n' + \
           '| ' + str(game_lines[0][0]) + ' | ' + str(game_lines[0][1]) + ' | ' + str(game_lines[0][2]) + ' |' + '\n' +\
           '-------------' + '\n' + \
           '| ' + str(game_lines[1][0]) + ' | ' + str(game_lines[1][1]) + ' | ' + str(game_lines[1][2]) + ' |' + '\n' +\
           '-------------' + '\n' + \
           '| ' + str(game_lines[2][0]) + ' | ' + str(game_lines[2][1]) + ' | ' + str(game_lines[2][2]) + ' |' + '\n' +\
           '-------------'


def clear_moves_so_far():
    # Function that clears the moves so far list
    moves_so_far.clear()


def save_record_of_game(moves_from_game, player_id_from_game, ai_difficulty_from_game):
    # Function that saves a record of the current game to a csv file
    try:
        with open("record_of_games.csv", "a+", newline='') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow([player_id_from_game, ai_difficulty_from_game, moves_from_game])
    except IOError:
        print("File not accessible")


def read_file_and_convert_to_dictionary():
    # Function that opens the record_of_games file and converts the contents to a dictionary
    records = {}

    f = open("record_of_games.csv", "r")
    reader = csv.reader(f)

    for row in reader:
        records[row[0]] = {'ai_level': row[1], 'moves': row[2]}

    return records


def search_player_records(searched_player_id):
    # Function that searches the dictionary for player id and if found returns a true value
    dic_of_records = read_file_and_convert_to_dictionary()

    if searched_player_id in dic_of_records:
        return True


def get_recorded_moves_from_file(searched_player_id):
    # Function that gets a record of moves from a previous game depending on the searched player
    # This list of moves is then converted into a list
    dic_of_records = read_file_and_convert_to_dictionary()

    if searched_player_id in dic_of_records:
        record = dic_of_records[searched_player_id]
        print('\n', record, '\n')

        moves = record['moves']
        # Getting rid of square brackets on either end of the record of moves
        moves = moves[1:-1]

        moves = str(moves)
        # Splitting into a list, separated at each comma
        list_of_moves = moves.split(',')

        # returning the list of moves from the searched game
        return list_of_moves
