# Imports
import random
import game

# This function will provide a computer move
# param 1: ai_level, value 0, 1 or 2
# param 2: eight_game_lines, a list of the 8 lines of the current game
# param 3: moves_already_made, a list of cells already taken


def get_whatever(ai_level, eight_game_lines, moves_already_made):

    # local variable set to an invalid cell in a game
    ai_move = 0

    # if statement that generates an ai move depending on selected difficulty
    if ai_level == 1:
        # return a random move

        # Convert the list of previous moves to a set
        set_of_moves_already_made = set(moves_already_made)

        # create a new set to hold the valid moves by
        # using a set difference - similar to subtraction
        # get set_cell_locations in game.py
        set_of_valid_moves = {1, 2, 3, 4, 5, 6, 7, 8, 9} - set_of_moves_already_made

        # for debug only print a message to say: here's the valid moves
        print(set_of_valid_moves)

        # create a list from the set of valid moves
        list_of_valid_moves = list(set_of_valid_moves)

        # calculate the length of list of valid moves - store in a local variable
        length_of_list_of_valid_moves = len(list_of_valid_moves)

        # calculate the last valid index in the list
        last_valid_index_of_the_list = length_of_list_of_valid_moves - 1

        # select a random index value from the list of valid move
        index_value = random.randint(0, last_valid_index_of_the_list)

        # set the random move to be the list item at that random index
        ai_move = list_of_valid_moves[index_value]

    elif ai_level == 2:
        # return a somewhat smart move

        # Convert the list of previous moves to a set
        set_of_moves_already_made = set(moves_already_made)

        # create a new set to hold the valid moves by
        # using a set difference - similar to subtraction
        # get set_cell_locations in game.py
        set_of_valid_moves = {1, 2, 3, 4, 5, 6, 7, 8, 9} - set_of_moves_already_made

        # for debug only print a message to say: here's the valid moves
        print(set_of_valid_moves)

        # create a list from the set of valid moves
        list_of_valid_moves = list(set_of_valid_moves)

        # if statement that causes the for loop to only run 66% of the time
        # this is to give the user a chance at winning
        if random.randint(0, 100) < 66:
            # loop that first checks for a possible ai win in the next move
            # if no win is found it then checks for a possible player win and then blocks that move
            for mark in [game.PLAYER_2, game.PLAYER_1]:
                for possible_move in list_of_valid_moves:
                    # making copy of the simple game board
                    # this copy can then be written into without altering the actual simple board
                    board_copy = game.simple_board[:]
                    # placing a mark in the corresponding cell depending on the current possible_move
                    board_copy[possible_move] = mark

                    # checking if this move returns a win
                    if game.simple_check_for_winner(board_copy, mark):
                        # if winning move then the move is returned
                        move = possible_move
                        return move

        # if statement is only reached if the above for loop does not find winning or losing move

        # first take the centre square
        if 5 in list_of_valid_moves:
            ai_move = 5

        # then take corners
        elif 1 in list_of_valid_moves or 3 in list_of_valid_moves or 7 in list_of_valid_moves \
                or 9 in list_of_valid_moves:
            # valid corner moves are placed in a set
            corner_move = {1, 3, 7, 9} - set_of_moves_already_made
            # the corner is then chosen by random.choice
            index_value = random.choice(tuple(corner_move))
            ai_move = index_value

        # then take edge squares
        elif 2 in list_of_valid_moves or 4 in list_of_valid_moves or 6 in list_of_valid_moves \
                or 8 in list_of_valid_moves:
            # valid edge square moves are placed in a set
            middle_cell_move = {2, 4, 6, 8} - set_of_moves_already_made
            # the edge square is then chosen by random.choice
            index_value = random.choice(tuple(middle_cell_move))
            ai_move = index_value

        else:
            print('AI could not generate a move, please restart game')

    elif ai_level == 3:
        # return the smartest possible move

        # Convert the list of previous moves to a set
        set_of_moves_already_made = set(moves_already_made)

        # create a new set to hold the valid moves by
        # using a set difference - similar to subtraction
        # get set_cell_locations in game.py
        set_of_valid_moves = {1, 2, 3, 4, 5, 6, 7, 8, 9} - set_of_moves_already_made

        # for debug only print a message to say: here's the valid moves
        print(set_of_valid_moves)

        # create a list from the set of valid moves
        list_of_valid_moves = list(set_of_valid_moves)

        # loop that first checks for a possible ai win in the next move
        # if no win is found it then checks for a possible player win and then blocks that move
        for mark in [game.PLAYER_2, game.PLAYER_1]:
            for possible_move in list_of_valid_moves:
                # making copy of the simple game board
                # this copy can then be written into without altering the actual simple board
                board_copy = game.simple_board[:]
                # placing a mark in the corresponding cell depending on the current possible_move
                board_copy[possible_move] = mark

                # checking if this move returns a win
                if game.simple_check_for_winner(board_copy, mark):
                    # if winning move then the move is returned
                    move = possible_move
                    return move

        # if statement is only reached if the above for loop does not find winning or losing move

        # first take the centre square
        if 5 in list_of_valid_moves:
            ai_move = 5

        # this defends against the player using a two opposite corners strategy
        # if X is in two opposite corners then take an edge square
        elif eight_game_lines[0][0] == game.PLAYER_1 and eight_game_lines[2][2] == game.PLAYER_1 or \
                eight_game_lines[2][0] == game.PLAYER_1 and eight_game_lines[0][2] == game.PLAYER_1:
            # valid edge square moves are placed in a set
            middle_cell_move = {2, 4, 6, 8} - set_of_moves_already_made
            # the edge square is then chosen by random.choice
            index_value = random.choice(tuple(middle_cell_move))
            ai_move = index_value

        # then take corners
        elif 1 in list_of_valid_moves or 3 in list_of_valid_moves or 7 in list_of_valid_moves \
                or 9 in list_of_valid_moves:
            # valid corner moves are placed in a set
            corner_move = {1, 3, 7, 9} - set_of_moves_already_made
            # the corner is then chosen by random.choice
            index_value = random.choice(tuple(corner_move))
            ai_move = index_value

        # then take edge squares
        elif 2 in list_of_valid_moves or 4 in list_of_valid_moves or 6 in list_of_valid_moves \
                or 8 in list_of_valid_moves:
            # valid edge square moves are placed in a set
            middle_cell_move = {2, 4, 6, 8} - set_of_moves_already_made
            # the edge square is then chosen by random.choice
            index_value = random.choice(tuple(middle_cell_move))
            ai_move = index_value

        else:
            print('AI could not generate a move, please restart game')

    else:
        # throw an exception
        print("ERROR CONDITION")

    # END if statement

    # return an ai move
    return ai_move

# END function get_whatever
